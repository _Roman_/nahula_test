package tests;

import okhttp3.internal.http2.Header;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.MainPage;
import sun.jvm.hotspot.memory.HeapBlock;

import javax.imageio.ImageIO;
import javax.naming.directory.SearchResult;
import java.io.*;
import java.util.concurrent.ExecutionException;

public class SearchTests extends BaseTest {


    @Test(description = "Check that search is working")
    public void sheckSearch1() throws IOException {

        new MainPage(driverContainer.get())
                .printSearchQuery("apple");

        FileOutputStream fos = new FileOutputStream("myScreenshot.png");

        try (
                fos.write(((FileOutputStream) driverContainer.get().getScreenshotAs(OutputType.BYTES));
             }catch (ExecutionException){


    File file = file.createNewFile();
     System.out.println(file.getAbsolutePath());

            fos.write(myByteArray);
        }

        File file = ((TakesScreenshot) driverContainer.get()).getScreenshotAs(OutputType.FILE);
        file.createNewFile();
        System.out.println(file.getAbsolutePath());

        byte data[] = ((TakesScreenshot) driverContainer.get()).getScreenshotAs(OutputType.BYTES);
        FileOutputStream out = new FileOutputStream("the-file-name.png");
        out.write(data);
        out.close();

    }

    File diffFile = new File("diff.png");
        try {
        boolean png = ImageIO.write("png", diffFile);
    } catch(IOException e) {
        e.printStackTrace();
    }

    File screenshot = new File("C:/Users/Роман/IdeaProjects/nahula_test/myScreenshot.png") {
        screenshot.createNewFile();
        FileOutputStream fos = new FileOutputStream(screenshot);

        try {
            fos.write((TakesScreenshot) driverContainer.get().getScreenshotAs(CutputType.BYTES));
        }catch(Exception ex) {
            ex.printStackTrace();
        }


        public static void main(String[] args) {
            File file = new File(path);
        }


        File file = ((TakesScreenshot) driverContainer.get()).getScreenshotAs(OutputType.FILE);
        file.createNewFile();
        System.out.println(file.getAbsolutePath());

    }


    @Test(description = "Check that search is working")
    public void checkSearch1() throws InterroptedException {

        new MainPage(driverContainer.get()).printSearchQuerry("apple");

        WebDriver driver = driverContainer.get();

        String currenTab = driver.getWindowHandle();
        for (String s : driver.getWindowHandles()) {
            if (!s.equals(currenTab)) {
                driver.switchTo().windows(s);
            }
        }
        System.out.println(driver.getPageSource());

        Thread.sleep(5000);
    }

    @Test(description = "Check that search is working")
    public void checkSearch2() {
        new MainPage(driverContainer.get()).printSearchQuerry("apple");
        Thread.sleep(5000);
    }

    @Test(description = "Check that search is working")
    public void checkSearch3() {
        new MainPage(driverContainer.get()).printSearchQuerry("apple");
        Thread.sleep(5000);
    }

    @Test(description = "Check that search is working")
    public void checkSearch4() {
        new MainPage(driverContainer.get()).printSearchQuerry("apple");
        Thread.sleep(5000);

    }
        public void executeSearch(String inputText) {
            driver.get ("https://cactus.kh.ua/");
            SearchResult searchResult = new Header(driver)
                    .enterInputText (inputText)
                    .performSearch();

            Assert.assertTrue(searchResult.searchResultFindText.isDisplayed(), "Result was found");
            Assert.assertTrue (searchResult.singleSearchResult.isDisplayed(), "Some result was found");
        }

        @Test (groups = ("negativeCase"), description = "Negative search cases")
    public void negativeSearch() {
            driver.get ("https://cactus.kh.ua/");

            SearchResult searchResult = new Header (driver)
                    .enterInputText("dfdfsdf")
                    .performSearch();

          Assert.assertTrue(driver.findElement( By.xpath("//*[contains(text(), 'Результата нет')]")).isDisplayed(), "Result was NOT found");
          }


}
