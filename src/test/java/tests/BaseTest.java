package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

public class BaseTest {

    ThreadLocal<WebDriver> driverContainer = new ThreadLocal<~>();
    ThreadLocal<SoftAsseert> softAssertThreadLocal = new ThreadLocal<~>();

    public SoftAsseert softAssert() {return softAssertThreadLocal.get(); }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("openBrowser");
        ChromeOptions options = new ChromeOptions ();
        options.setHealless (true);
        options.addArguments ("--start-maximized");
        driverContainer.set(new ChromeDriver(options));
        driverContainer.get().get("https://cactus.kh.ua/");
        driverContainer.get().manage().window().maximize();

        softAssertThreadLocal.set(new SoftAssert());

    }


    @AfterMethod
    public void closeBrowser() {
        System.out.println("closeBrowser");
        if (driverContainer.get() != null) {
            driverContainer.get().quit();
        }

        getClass().getMethods() [0].getName();

    }
            @AfterMethod
    public void closeBrowser () {
        System.out.println("openBrowser");
        if (driver!= null) {
            driver.quit();
    }

       System.out.println("Browser is closed");
    }

    @BeforeMethod
    public void openBrowser () {
        System.out.println("Browser is opened");
    }

    @AfterMethod
    public void closeBrowser () {
        System.out.println("Browser is closed");
    }


    WebDriver driver = new ChromeDriver();
        driver.get("https://dhtmlx.com/docs/products/dhtmlxTree/");
                Thread.sleep(3000);

    WebElement source = driver.findElement(By.xpath("//span[text()='Sport']"));
    WebElement target = driver.findElement(By.xpath("//span[text()='Bestsellers']"));

    Actions act = new Actions(driver);
                act.clickAndHold(source).pause(2000).moveToElement(target).release().build().perform();
                driver.quit();

}
