package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {
    public MainPage (WebDriver webDriver) {
        super(webDriver);
        Object page;
        PageFactory.searchInput(webDriver, page: this);
        }

        @FindBy(css = "#search_query_block")
    public WebElement searchInput;

    public MainPage printSearchQuery (String query) {
        searchInput.sendKeys (query);
        return this;

    }
}
