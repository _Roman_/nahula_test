package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sun.jvm.hotspot.debugger.Page;

import javax.naming.directory.SearchResult;

public class Header extends BasePage{

public Header (WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
}

    @FindBy (xpath = "//*[@id='txtGlobalSearch']")
    WebElement searthField;

    public Header enterInputText(String inputText) {
        searthField.sendKeys(inputText);
        return this;
    }

    public SearchResult performSearch() {
        searthField.sendKeys(Keys.ENTER);
        SearchResult searchResult = new SearchResult();
                return searchResult;

    }
}
