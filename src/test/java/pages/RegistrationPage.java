package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.seleniumhq.jetty9.server.Authentication;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RegistrationPage {
    private static String URL_MATCH = "registration";

    private WebDriver driver;

    @FindBy(id = "userLogin")
    private WebElement login;

    @FindBy(id = "regPassword")
    private WebElement password;

    @FindBy(id = "passwordConfirmation")
    private WebElement passwordConfirm;

    @FindBy(id = "UserEmail")
    private WebElement email;

    @FindBy(id = "submitRegistration")
    private WebElement bSubmitRegister;

    @FindBy(id = "error-message")
    private WebElement registerError;
    private Authentication.User user;

    public RegistrationPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(URL_MATCH)) {
            throw new IllegalStateException(
                    "This is not the page you are expected"
            );
        }

        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private void registerUser(Authentication.User user) {
        this.user = user;
        System.out.println(driver.getTitle());
        login.sendKeys(user.login);
        password.sendKeys(user.password);
        passwordConfirm.sendKeys(user.passwordConfirmation);
        email.sendKeys(user.email);

        bSubmitRegister.click();
    }

    @Test
    private void registerUserTest() {
        driver.get("http://HOST_NAME/registration");
        Authentication.User user = Authentication.User.createValidUser();
        user.email = "not_valid_email";

        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage
                .registerUserError(user)
                .checkErrorMessage(errorMessage)
        ;

        user = Authentication.User.createValidUser();

        registrationPage
                .registerUserSuccess(user);
    }

    public HomePage registerUserSuccess(Authentication.User user) {
        registerUser(user);
        return new HomePage(driver);
    }

    public RegistrationPage registerUserError(Authentication.User user) {
        registerUser(user);
        return new RegistrationPage(driver);
    }

    public RegistrationPage checkErrorMessage(String errorMessage) {
        Assert.assertTrue(registerError.isDisplayed(),
                "Error message should be present");
        Assert.assertTrue(registerError.getText().contains(errorMessage),
                "Error message should contains " + errorMessage);
        return this;
    }
}
